var async = require('async');
var Netsuite = require('../netsuite.js');

exports.index = function (req, res) {
  Netsuite.reserve(function (err, connObj) {
    if (err) {
      console.log("Reserve Error: " + err);
    }
    if (connObj) {
      console.log("Using connection: " + connObj.uuid);
      // Grab the Connection for use.
      var conn = connObj.conn;

      async.series([
        function (callback) {
          conn.createStatement(function (err, statement) {
            if (err) {
              callback(err);
            }
            else {
              statement.executeQuery("SELECT IM3_DATA_GENERAL_READINGS.CURRENT_DAILY_AVERAGE, IM3_DATA_GENERAL_READINGS.CURRENT_DAILY_AVERAGE_CHANGE, IM3_DATA_GENERAL_READINGS.CURRENT_READING, IM3_DATA_GENERAL_READINGS.DATE_CREATED, IM3_DATA_GENERAL_READINGS.DIFFERENCE_FROM_PRIOR, IM3_DATA_GENERAL_READINGS.MANUAL_OVERRIDE, IM3_DATA_GENERAL_READINGS.PRIOR_READING, IM3_DATA_GENERAL_READINGS.PRIOR_READING_DATE FROM IM3_DATA_GENERAL_READINGS WHERE IM3_DATA_GENERAL_READINGS.SITE_SYSTEM_ID = " + req.params.siteSystemID + "'", function (err, resultset) {
                if (err) {
                  callback(err);
                }
                else {
                  resultset.toObjArray(function (err, results) {
                    if (err) {
                      callback(err);
                    }
                    else {
                      callback(null, results);
                    }
                  });
                }
              });
            }
          });
        }
      ], function (err, results) {
        if (err) {
          console.log("Aysnc Error: " + err);
        }
        else {
          Netsuite.release(connObj, function (err) {
            if (err) {
              console.log("Release Connection Error: " + err);
            }
          });
          res.json(results[0]);
        }
      });
    }
  });
};