var async = require('async');
var Netsuite = require('../netsuite.js');

exports.index = function (req, res) {
  Netsuite.reserve(function (err, connObj) {
    if (err) {
      console.log("Reserve Error: " + err);
    }
    if (connObj) {
      console.log("Using connection: " + connObj.uuid);
      // Grab the Connection for use.
      var conn = connObj.conn;

      async.series([
        function (callback) {
          conn.createStatement(function (err, statement) {
            if (err) {
              callback(err);
            }
            else {
              statement.executeQuery("SELECT RBQSample.RBQ__SAMPLE_NUMBER AS 'Sample Number', RBQSample.LAB_RESULTS__DATE_OF_SAMPLING AS 'Sample Date', RBQSample.LEGIONELLA_SPP__TOTAL_CFUL, RBQSample.L__PNEUMOPHILA__SEROGROUPE_1, RBQSample.L__PNEUMOPHILA__SEROGROUPE_2, RBQSample.LEGIONELLA_SPECIES AS 'Species', RBQSample.RESULTS_UNITS AS 'Units', ServiceRecord.SERVICE_DATE, Employee.EMAIL, Hardness.READING AS Hardness, Calcium.READING AS Calcium, pH.READING AS pH, Conductivity.READING AS Conductivity, Chloride.READING AS Chloride, TAlkalinity.READING AS 'T Alkalinity', PAlkalinity.READING AS 'P Alkalinity', Tracer.READING AS Tracer, THalogen.READING AS THalogen, FHalogen.READING AS FHalogen, Bacteria.READING AS Bacteria FROM RBQ__SAMPLE RBQSample INNER JOIN SERVICE_RECORD ServiceRecord ON RBQSample.PREVIOUS_SERVICE_RECORD_ID = ServiceRecord.SERVICE_RECORD_ID LEFT JOIN IM3_DATA_WATER_ANALYSIS Hardness ON ServiceRecord.SERVICE_RECORD_ID = Hardness.SERVICE_RECORD_ID AND Hardness.TEST_ID = 21 LEFT JOIN IM3_DATA_WATER_ANALYSIS Calcium ON ServiceRecord.SERVICE_RECORD_ID = Calcium.SERVICE_RECORD_ID AND Calcium.TEST_ID = 24 LEFT JOIN IM3_DATA_WATER_ANALYSIS pH ON ServiceRecord.SERVICE_RECORD_ID = pH.SERVICE_RECORD_ID AND pH.TEST_ID = 26 LEFT JOIN IM3_DATA_WATER_ANALYSIS Conductivity ON ServiceRecord.SERVICE_RECORD_ID = Conductivity.SERVICE_RECORD_ID AND Conductivity.TEST_ID = 27 LEFT JOIN IM3_DATA_WATER_ANALYSIS Chloride ON ServiceRecord.SERVICE_RECORD_ID = Chloride.SERVICE_RECORD_ID AND Chloride.TEST_ID = 28 LEFT JOIN IM3_DATA_WATER_ANALYSIS TAlkalinity ON ServiceRecord.SERVICE_RECORD_ID = TAlkalinity.SERVICE_RECORD_ID AND TAlkalinity.TEST_ID = 37 LEFT JOIN IM3_DATA_WATER_ANALYSIS PAlkalinity ON ServiceRecord.SERVICE_RECORD_ID = PAlkalinity.SERVICE_RECORD_ID AND PAlkalinity.TEST_ID = 38 LEFT JOIN IM3_DATA_WATER_ANALYSIS Tracer ON ServiceRecord.SERVICE_RECORD_ID = Tracer.SERVICE_RECORD_ID AND Tracer.TEST_ID = 40 LEFT JOIN IM3_DATA_WATER_ANALYSIS THalogen ON ServiceRecord.SERVICE_RECORD_ID = THalogen.SERVICE_RECORD_ID AND THalogen.TEST_ID = 42 LEFT JOIN IM3_DATA_WATER_ANALYSIS FHalogen ON ServiceRecord.SERVICE_RECORD_ID = FHalogen.SERVICE_RECORD_ID AND FHalogen.TEST_ID = 43 LEFT JOIN IM3_DATA_WATER_ANALYSIS Bacteria ON ServiceRecord.SERVICE_RECORD_ID = Bacteria.SERVICE_RECORD_ID AND Bacteria.TEST_ID = 45 INNER JOIN EMPLOYEES Employee ON ServiceRecord.SERVICED_BY_ID = Employee.EMPLOYEE_ID WHERE RBQSample.RBQ__SAMPLE_NUMBER = '" + req.params.sampleNum + "'", function (err, resultset) {
                if (err) {
                  callback(err);
                }
                else {
                  resultset.toObjArray(function (err, results) {
                    if (err) {
                      callback(err);
                    }
                    else {
                      callback(null, results);
                    }
                  });
                }
              });
            }
          });
        }
      ], function (err, results) {
        if (err) {
          console.log("Aysnc Error: " + err);
        }
        else {
          Netsuite.release(connObj, function (err) {
            if (err) {
              console.log("Release Connection Error: " + err);
            }
          });
          res.json(results[0]);
        }
      });
    }
  });
};

exports.samples = function (req, res) {
  Netsuite.reserve(function (err, connObj) {
    if (err) {
      console.log("Reserve Error: " + err);
    }
    if (connObj) {
      console.log("Using connection: " + connObj.uuid);
      // Grab the Connection for use.
      var conn = connObj.conn;

      async.series([
        function (callback) {
          conn.createStatement(function (err, statement) {
            if (err) {
              callback(err);
            }
            else {
              statement.executeQuery("SELECT TOP 5 * FROM RBQ__SAMPLE", function (err, resultset) {
                if (err) {
                  callback(err);
                }
                else {
                  resultset.toObjArray(function (err, results) {
                    if (err) {
                      callback(err);
                    }
                    else {
                      callback(null, results);
                    }
                  });
                }
              });
            }
          });
        }
      ], function (err, results) {
        if (err) {
          console.log("Aysnc Error: " + err);
        }
        else {
          Netsuite.release(connObj, function (err) {
            if (err) {
              console.log("Release Connection Error: " + err);
            }
          });
          res.json(results[0]);
        }
      });
    }
  });
};