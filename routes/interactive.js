var async = require('async');
var Netsuite = require('../netsuite.js');
var moment = require('moment');
var _ = require('lodash');

exports.index = function (req, res) {
    var jsonResponse = {
        "success": true
    };

    async.parallel([
        function (callback) {
            getServiceRecordDetails(jsonResponse, req.params.servicRecordID, req.params.siteSystemID, callback);
        },
        function (callback) {
            getWaterAnalysis(jsonResponse, req.params.servicRecordID, req.params.siteSystemID, callback);
        },
        function (callback) {
            getReportSettings(jsonResponse, 1, callback);
        }
    ], function (err, results) {
        if (err) {
            console.log("Aysnc Error: " + err);
        }
        res.json(jsonResponse);
    });
};

var getReportSettings = function (jsonResponse, reportSettingId, callback) {
    Netsuite.reserve(function (err, connObj) {
        if (err) {
            console.log("Reserve Error: " + err);
        }
        if (connObj) {
            console.log("Using connection: " + connObj.uuid);
            // Grab the Connection for use.
            var conn = connObj.conn;
            conn.createStatement(function (err, statement) {
                if (err) {
                    callback(err);
                }
                else {
                    statement.executeQuery('SELECT * FROM IM3_M_REPORT_SETTINGS WHERE  IM3_M_REPORT_SETTINGS.IM3_M_REPORT_SETTINGS_ID = ' + reportSettingId + ';', function (err, resultset) {
                        if (err) {
                            callback(err);
                        }
                        else {
                            resultset.toObjArray(function (err, results) {
                                if (err) {
                                    callback(err);
                                }
                                else {
                                    Netsuite.release(connObj, function (err) {
                                        if (err) {
                                            console.log("Release Connection Error: " + err);
                                        }
                                    });

                                    jsonResponse.ReportSetting = [{
                                        id: reportSettingId,
                                        recordtype: 'customrecord_im3m_reportsetting',
                                    }];

                                    _.each(results, function (result) {
                                        jsonResponse.ReportSetting[0].columns = {
                                            custrecord_im3m_reportset_cinfo: result.COMPANY_INFO__FOOTER,
                                            custrecord_im3m_reportset_d1_g1: result.DASHBOARD__GRAPH_1,
                                            custrecord_im3m_reportset_d1_g2: result.DASHBOARD__GRAPH_2,
                                            custrecord_im3m_reportset_d1_g3: result.DASHBOARD__GRAPH_3,
                                            custrecord_im3m_reportset_d1_l1: result.DASHBOARD__LABEL_1,
                                            custrecord_im3m_reportset_d1_l10: result.DASHBOARD__LABEL_10,
                                            custrecord_im3m_reportset_d1_l11: result.DASHBOARD__LABEL_11,
                                            custrecord_im3m_reportset_d1_l2: result.DASHBOARD__LABEL_2,
                                            custrecord_im3m_reportset_d1_l3: result.DASHBOARD__LABEL_3,
                                            custrecord_im3m_reportset_d1_l4: result.DASHBOARD__LABEL_4,
                                            custrecord_im3m_reportset_d1_l5: result.DASHBOARD__LABEL_5,
                                            custrecord_im3m_reportset_d1_l6: result.DASHBOARD__LABEL_6,
                                            custrecord_im3m_reportset_d1_l7: result.DASHBOARD__LABEL_7,
                                            custrecord_im3m_reportset_d1_l8: result.DASHBOARD__LABEL_8,
                                            custrecord_im3m_reportset_d1_l9: result.DASHBOARD__LABEL_9,
                                        };
                                    });
                                    callback(null, results);
                                }
                            });
                        }
                    });
                }
            });
        }
    });
}

var getServiceRecordDetails = function (jsonResponse, servicRecordID, siteSystemID, callback) {
    Netsuite.reserve(function (err, connObj) {
        if (err) {
            console.log("Reserve Error: " + err);
        }
        if (connObj) {
            console.log("Using connection: " + connObj.uuid);
            // Grab the Connection for use.
            var conn = connObj.conn;
            conn.createStatement(function (err, statement) {
                if (err) {
                    callback(err);
                }
                else {
                    statement.executeQuery('SELECT SERVICE_RECORD.SERVICE_DATE AS "date", CUSTOMERS.COMPANYNAME AS "customerName", CUSTOMERS.CUSTOMER_ID AS "customerId", IM3_C_SITE_SYSTEM.IM3_C_SITE_SYSTEM_NAME   AS "systemName", SERVICE_RECORD.SERVICE_RECORD_ID AS "serviceRecordId", IM3_C_SITE_SYSTEM.IM3_C_SITE_SYSTEM_ID AS "siteSystemId", SERVICE_RECORD.STATUS_ID    AS "serviceStatus", IM3_DATA_DATA_ENTRY_STATUS.STATUS_ID AS "dataEntryStatusId", IM3_DATA_DATA_ENTRY_STATUS.IM3_DATA_DATA_ENTRY_STATUS_ID  AS "dataEntryStatusRecordId", IM3_DATA_ENTRY_STATUS_TYPE.IM3_DATA_ENTRY_STATUS_TYPE_NAM AS "dataEntryStatusName" FROM SERVICE_RECORD INNER JOIN CUSTOMERS ON ( SERVICE_RECORD.CUSTOMER_ID = CUSTOMERS.CUSTOMER_ID) INNER JOIN IM3_DATA_DATA_ENTRY_STATUS ON ( SERVICE_RECORD.SERVICE_RECORD_ID = IM3_DATA_DATA_ENTRY_STATUS.SERVICE_RECORD_ID) INNER JOIN IM3_DATA_ENTRY_STATUS_TYPE ON ( IM3_DATA_DATA_ENTRY_STATUS.STATUS_ID = IM3_DATA_ENTRY_STATUS_TYPE.IM3_DATA_ENTRY_STATUS_TYPE_ID) INNER JOIN IM3_C_SITE_SYSTEM ON ( IM3_DATA_DATA_ENTRY_STATUS.SITE_SYSTEM_ID = IM3_C_SITE_SYSTEM.IM3_C_SITE_SYSTEM_ID) WHERE SERVICE_RECORD.SERVICE_RECORD_ID = ' + servicRecordID + ' AND IM3_C_SITE_SYSTEM.IM3_C_SITE_SYSTEM_ID = ' + siteSystemID + ';', function (err, resultset) {
                        if (err) {
                            callback(err);
                        }
                        else {
                            resultset.toObjArray(function (err, results) {
                                if (err) {
                                    callback(err);
                                }
                                else {
                                    Netsuite.release(connObj, function (err) {
                                        if (err) {
                                            console.log("Release Connection Error: " + err);
                                        }
                                    });

                                    jsonResponse.date = moment(results[0].date, 'YYYY-MM-DD HH:mm:ss.S').utcOffset(-4).format('MM/DD/YYYY h:mm a');
                                    jsonResponse.customerName = results[0].customerName;
                                    jsonResponse.customerId = results[0].customerId;
                                    jsonResponse.roleId = 3;
                                    jsonResponse.systemName = results[0].systemName;
                                    jsonResponse.serviceRecordId = results[0].serviceRecordId;
                                    jsonResponse.siteSystemId = results[0].siteSystemId;
                                    jsonResponse.settingId = 1;
                                    jsonResponse.createdBy = '';
                                    jsonResponse.serviceStatus = results[0].serviceStatus;
                                    jsonResponse.DataStatus = [{
                                        id: results[0].dataEntryStatusRecordId,
                                        recordtype: 'customrecord_im3d_data_status',
                                        columns: {
                                            custrecord_im3d_data_status: {
                                                name: results[0].dataEntryStatusName,
                                                internalid: results[0].dataEntryStatusId
                                            }
                                        }
                                    }];
                                    callback(null, results);
                                }
                            });
                        }
                    });
                }
            });
        }
    });
}

var getWaterAnalysis = function (jsonResponse, servicRecordID, siteSystemID, callback) {
    var newSamplePoint, foundSamplePoint, newTestLimit, foundTestLimit;

    Netsuite.reserve(function (err, connObj) {
        if (err) {
            console.log("Reserve Error: " + err);
        }
        if (connObj) {
            console.log("Using connection: " + connObj.uuid);
            // Grab the Connection for use.
            var conn = connObj.conn;
            conn.createStatement(function (err, statement) {
                if (err) {
                    callback(err);
                }
                else {
                    statement.executeQuery('SELECT IM3_C_SAMPLE_POINTS.IM3_C_SAMPLE_POINTS_ID AS "samplePointId", IM3_C_SAMPLE_POINTS.NUMBER_0 AS "samplePointNumber", IM3_C_SAMPLE_POINTS.SITE_SYSTEM_ID AS "samplePointSiteSystem", IM3_C_SAMPLE_POINTS.TYPE_ID AS "samplePointTypeId", IM3_C_SAMPLE_POINTS.DESCRIPTION__E AS "samplePointDescriptionEnglish", IM3_C_SAMPLE_POINTS.DESCRIPTION__F AS "samplePointDescriptionFrench", IM3_M_SAMPLE_POINT_TYPES.IM3_M_SAMPLE_POINT_TYPES_NAME AS "samplePointTypeName", IM3_C_TEST_LIMITS.IM3_C_TEST_LIMITS_ID AS "testLimitId", IM3_C_TEST_LIMITS.NUMBER_0 AS "testLimitNumber", IM3_M_TESTS.IM3_M_TESTS_ID AS "testId", IM3_M_TESTS.IM3_M_TESTS_NAME AS "testName", IM3_M_TESTS.NUMBER_0 AS "testNumber", IM3_M_TESTS.ENGLISH_NAME AS "testNameEnglish", IM3_M_TESTS.FRENCH_NAME AS "testNameFrench", IM3_DATA_READING_FORMAT.IM3_DATA_READING_FORMAT_NAME AS "testReadingFormatName", IM3_DATA_READING_FORMAT.IM3_DATA_READING_FORMAT_ID AS "testReadingFormatId", IM3_DATA_WATER_ANALYSIS.IM3_DATA_WATER_ANALYSIS_ID AS "waterAnalysisRecordId", IM3_DATA_WATER_ANALYSIS.READING AS "waterAnalysisReading", IM3_DATA_WATER_ANALYSIS.COMMENT_0 AS "waterAnalysisComment" FROM IM3_C_TEST_LIMITS INNER JOIN IM3_C_SAMPLE_POINTS ON ( IM3_C_TEST_LIMITS.SAMPLE_POINT_ID = IM3_C_SAMPLE_POINTS.IM3_C_SAMPLE_POINTS_ID) INNER JOIN IM3_M_SAMPLE_POINT_TYPES ON (IM3_C_SAMPLE_POINTS.TYPE_ID = IM3_M_SAMPLE_POINT_TYPES.IM3_M_SAMPLE_POINT_TYPES_ID) INNER JOIN IM3_M_TESTS ON (IM3_C_TEST_LIMITS.TEST_ID = IM3_M_TESTS.IM3_M_TESTS_ID) INNER JOIN IM3_DATA_READING_FORMAT ON (IM3_M_TESTS.TEST_READING_FORMAT_ID = IM3_DATA_READING_FORMAT.IM3_DATA_READING_FORMAT_ID) LEFT JOIN IM3_DATA_WATER_ANALYSIS ON (IM3_C_TEST_LIMITS.IM3_C_TEST_LIMITS_ID = IM3_DATA_WATER_ANALYSIS.TEST_LIMIT_ID AND IM3_DATA_WATER_ANALYSIS.SERVICE_RECORD_ID = ' + servicRecordID + ') WHERE IM3_C_SAMPLE_POINTS.SITE_SYSTEM_ID = ' + siteSystemID + ' AND IM3_C_SAMPLE_POINTS.IS_INACTIVE = \'F\' AND IM3_C_TEST_LIMITS.IS_INACTIVE = \'F\' ORDER BY "samplePointNumber" ASC, "testLimitNumber" ASC, "testNumber" ASC;', function (err, resultset) {
                        if (err) {
                            callback(err);
                        }
                        else {
                            resultset.toObjArray(function (err, results) {
                                if (err) {
                                    callback(err);
                                }
                                else {
                                    Netsuite.release(connObj, function (err) {
                                        if (err) {
                                            console.log("Release Connection Error: " + err);
                                        }
                                    });
                                    jsonResponse.WaterAnalysis = {
                                        Samples: []
                                    };

                                    _.each(results, function (result) {
                                        newSamplePoint = {
                                            id: result.samplePointId,
                                            recordtype: 'customrecord_im3c_samplept',
                                            columns: {
                                                custrecord_im3c_samplept_num: result.samplePointNumber,
                                                custrecord_im3c_samplept_system: {
                                                    name: '',
                                                    internalid: siteSystemID
                                                },
                                                custrecord_im3c_samplept_type: {
                                                    name: result.samplePointTypeName,
                                                    internalid: result.samplePointTypeId
                                                },
                                                custrecord_im3c_samplept_desceng: result.samplePointDescriptionEnglish,
                                                custrecord_im3c_samplept_descfr: result.samplePointDescriptionFrench
                                            }
                                        };

                                        foundSamplePoint = _.findWhere(jsonResponse.WaterAnalysis.Samples, newSamplePoint);
                                        if (!foundSamplePoint) {
                                            newSamplePoint.Tests = [];
                                            jsonResponse.WaterAnalysis.Samples.push(newSamplePoint);
                                            foundSamplePoint = _.findWhere(jsonResponse.WaterAnalysis.Samples, newSamplePoint);
                                        }

                                        newTestLimit = {
                                            id: result.testLimitId,
                                            recordtype: 'customrecord_im3c_testlimit',
                                            columns: {
                                                custrecord_im3c_testlimit_num: result.testLimitNumber,
                                                custrecord_im3c_testlimit_test: {
                                                    name: result.testName,
                                                    internalid: result.testId
                                                },
                                                custrecord_im3m_test_reading_format: {
                                                    name: result.testReadingFormatName,
                                                    internalid: result.testReadingFormatId
                                                },
                                                custrecord_im3m_desceng: result.testNameEnglish,
                                                custrecord_im3m_descfr: result.testNameFrench
                                            },
                                            Data: null
                                        };

                                        if (result.waterAnalysisRecordId > 0) {
                                            newTestLimit.Data = [{
                                                id: result.waterAnalysisRecordId,
                                                recordtype: 'customrecord_im3d_wa',
                                                columns: {
                                                    custrecord_im3d_wa_reading: result.waterAnalysisReading,
                                                    custrecord_im3d_wa_comment: result.waterAnalysisComment || ''
                                                }
                                            }]
                                        }
                                        foundSamplePoint.Tests.push(newTestLimit);
                                        // foundTestLimit = _.findWhere(foundSamplePoint.Tests, newTestLimit);
                                        // if (!foundTestLimit) {
                                        //     foundSamplePoint.Tests.push(newTestLimit);
                                        //     foundTestLimit = _.findWhere(foundSamplePoint.Tests, newTestLimit);
                                        // }
                                    });

                                    callback(null, results);
                                }
                            });
                        }
                    });
                }
            });
        }
    });
}