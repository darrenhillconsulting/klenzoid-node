var googleapis = require('googleapis');
var calendar = googleapis.calendar('v3');

exports.create = function (req, res) {
    var authClient = new googleapis.auth.JWT('800859016422-bm4bs8tiursvn10trav7hj66f4hl1ils@developer.gserviceaccount.com', null, '-----BEGIN PRIVATE KEY-----\nMIIEvwIBADANBgkqhkiG9w0BAQEFAASCBKkwggSlAgEAAoIBAQDPfi1P6vmYhhjj\nnudum7LrWcfxQAZ4BNvJHjaUMPQd8d6VZICUm1SQSij8dhO0lZAtaiFRWC1hY/5O\nzpfvAvNlEr9ku08T15fVhcCTdgq1FS2Wy+/NzqTafupyW6IziS5YYlmqUxvEdcUO\neS5ZJ0oi5+NMkL6nSrYt/99XcOlw53qIYoUvZSjE3ACEZNm3WcGf2xoKFAO8285U\nWA1CAldSgNNg4mgpgQ6ojyTjcI9ZRs8Bm6sFU57sgc1mR5jdB09ed6O6JlffmO5M\nmDmzETBllsmR1XWlLPIhwHuydNkFNT1OU+5tSvqI9eP0WIvAsZKhVTF1/gZ2WAiC\nxNLw0oWDAgMBAAECggEADIPu1cA1l+aWv7wI95n6m8mJjtquyTR50VK6NgwR3XCG\nXXp3av9HhYCDol64546R6LtbER4BdrtiTnyVm3wRFEqiMsA9j9PUO7Panspi8PM7\nEbpxV3ecZ/V5zLYMPZGLsxcTk3t51ErKPQpAKm59QUe9ZRPh/BM8k9+4J/0Oh5nC\n6ZJj29otcC4mfESBmMlo5LAHiwWI0a4RB4ciGyBKShHNj6JYk+P4CzWrO4hyt76V\nOZiQvLkh2XDqSJIQxIGS43qPmw8rMIl5pkd568WIbLSLmY7EhYKgJCPb7+ettzTz\nlJCo+42/hojwnYKPh1WmJLPugbdNw6/jNxcOeNHF+QKBgQDzD52sMJOJlufAk0l5\nwMYEAGF9hf809hwSJMWc8T1ERurIhRIv5kMFOW/QyEZLQxG+jKoKOTh5Fnl8Ht1C\nKwws4iZjCXrrlwcjsTbnEbw3f5Kog/ia244Cv54RP8jk2UI3p4+SqMSsN8ec0dy6\nxlnu3xGRYPJN/EVEx/q3yX/i1wKBgQDaidii+BTTKMzEamebdaM9Qq14T69F25wB\ngZ0VT0Ext/lHtKOV9NigHwgdkHfEwReJB7Sqn0zISL1HA+lxRmQWLXlmJYNR4Uwp\nTaitJmLiFlRptZHbORCc9TTeIFIXFdKXM1jzCQ8kAofltIIYexNZx3CvdA/2tnP6\nIVpr+7kJNQKBgQDCQebIKYKg4mSSeqSslC8uH4fLJH7VOyUwMwdk6UEIAo2+eOwS\nMDu8Py1odcgIerAB1LZJuXWzAQ1S7+/x5Cm07YG0gW0mQOxJ0GaKwpfk2ybBW8gj\nK/oRpEeWjZ3nmOPdSyPqGMwmv+XfJbL9620RBmMqB4tgXJCX6iQtIvP8vQKBgQCU\nfSk3wnzT5VsM8oend80M6OtZXjtnqgpcc6c6PXm8wJaehDzKvW0bjzpCbFEU3WaS\ncyvs6wSO6e3B/QR3NS74armcEE0kK4NGZUABNp3lpOqCUR7j5+hD3dCoRSnCCc/n\ncLo1mKgy/Y09JONooufJDtygOqpGJj6zbEo2KPnXKQKBgQCInNwbDz+cPyryQdA9\nwDfANXPCgjMHoFcWovkL8+U7VrKppjW/hKtGrJP8aQyccVGIFeLKtOs57+DfDQ7D\nKm94kd2v3vy4LiWI1Sdnt7tDYIqYcwVl9PjR7nTG9zs4ZKg3l9AvWhgXKQWvY7ij\nkNuEvZKCymOQ0w8bo3G6C02Jug\u003d\u003d\n-----END PRIVATE KEY-----\n', ['https://www.googleapis.com/auth/calendar'], req.body.user);

    authClient.authorize(function (err, tokens) {
        if (err) {
            throw err;
        }
        else {
            calendar.events.insert({
                auth: authClient,
                calendarId: req.body.user,
                resource: req.body.event
            }, function (err, event) {
                if (err) {
                    //console.log('There was an error contacting the Calendar service: ' + err);
                    res.send('There was an error contacting the Calendar service: ' + err);
                }
                //console.log('Event created: %s', event.htmlLink);
                res.json(event);
            });
        }
    });
};