/*global Backbone*/
var RBQSample = Backbone.Model.extend({
    idAttribute: 'RBQ__SAMPLE_ID'
});

var RBQSampleCollection = Backbone.Collection.extend({
    model: RBQSample,
    url: '/samples'
});