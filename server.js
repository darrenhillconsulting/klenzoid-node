//
// # SimpleServer
//
// A simple chat server using Socket.IO, Express, and Async.
//
var http = require('http');
var path = require('path');
var express = require('express');
var routes = require('./routes');
var rbqSample = require('./routes/rbqSample');
var generalReading = require('./routes/generalReading');
var interactive = require('./routes/interactive');
var calendar = require('./routes/calendar');

var app = express();

app.configure(function () {
  app.set('port', process.env.PORT || 3000);
  app.set('views', __dirname + '/views');
  app.set('view engine', 'jade');
  app.use(express.favicon());
  app.use(express.logger('dev'));
  app.use(express.bodyParser());
  app.use(express.methodOverride());
  app.use(app.router);
  app.use(express.static(path.join(__dirname, 'public')));
});

//var server = http.createServer(app);

//app.use(express.static(__dirname + '/public'));

app.get('/', routes.index);
app.get('/klz/rbq/:sampleNum', rbqSample.index);
app.get('/samples', rbqSample.samples);
app.get('/klz/gr/:siteSystemID', generalReading.index);
app.post('/calendar/create', calendar.create);
app.get('/interactive/:servicRecordID/:siteSystemID', interactive.index);



http.createServer(app).listen(app.get('port'), process.env.IP || '0.0.0.0', function () {
  console.log('Express server listening on port ' + app.get('port'));
});