var path = require('path');
var java = require('java');
var JDBC = require('jdbc');
var config = require('./config.json');

function isJvmCreated() {
  return typeof java.onJvmCreated !== 'function';
}

if (!isJvmCreated()) {
  java.options.push("-Xrs");
  java.classpath.push.apply(java.classpath, [path.resolve(__dirname, './NQjc.jar')]);
}

java.newInstanceSync(config.drivername);

var Netsuite = new JDBC(config);

Netsuite.initialize(function (err) {
  if (err) {
    console.log(err);
  }
});

module.exports = Netsuite;
